# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  |                                 |
| odkud jsem čerpal inspiraci                   | moje hlava|
| odkaz na video                                | https://www.youtube.com/watch?v=-r-60GvAe6Q                    |
| jak se mi to podařilo rozplánovat             |                                 |
| proč jsem zvolil tento design                 |                                 |
| zapojení                                      | *URL obrázku zapojení*          |
| z jakých součástí se zapojení skládá          |                                 |
| realizace                                     | *URL obrázku hotového produktu* |
| UI                                            | *URL obrázku UI*                |
| co se mi povedlo                              |                                 |
| co se mi nepovedlo/příště bych udělal/a jinak | design (vždy to může být lepší) |
| zhodnocení celé tvorby (návrh známky)         |                      9/10 1;1-  |
