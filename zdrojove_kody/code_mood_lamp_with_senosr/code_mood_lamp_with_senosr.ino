#include <DHT.h>
#include <Adafruit_NeoPixel.h>
#define DHTPIN D4      // Pin where the DHT11 is connected to (adjust pin according to your wiring)
#define DHTTYPE DHT11  // DHT 11
#define LED_PIN     D6
#define LED_COUNT   20  // Change this to match the number of LEDs in your strip

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  Serial.println("DHT11 test");

  dht.begin();
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void loop() {
  // Fill the entire strip with a solid color
  colorWipe(strip.Color(255, 0, 0), 50); // Red
  colorWipe(strip.Color(0, 255, 0), 50); // Green
  colorWipe(strip.Color(0, 0, 255), 50); // Blue

  delay(2000);  // Delay between sensor readings

  float humidity = dht.readHumidity();    // Read humidity value
  float temperature = dht.readTemperature(); // Read temperature value in Celsius

  // Check if any reads failed and exit early (to try again).
  if (isnan(humidity) || isnan(temperature)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }



  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.print("%\t");
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.println("°C");
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
    strip.show();
    delay(wait);
  }
}